/* WIW - Where It Work by Albert Jurasik*/

//Global variables 
var vector;
var lvl = 1;
var shotNr = 0;
var pos = [];
var now;
var before;
var between = 0;
var clock = new Date();
var start = true;
var bottom = 1;
var width;
var height;
var offsetBullet;
var wSize = [];
var count = 12;

$(document).ready(function () {
    //functions
    
    createEnemies();
    
    screenWidth();
    moveAndShot();
    checkWindowSize();
    
});


function checkWindowSize() {
    wSize[0] = $(window).width();
    wSize[1] = $(window).height();
}


//Set ship position 
function screenWidth() {
    var x = $(document).width();
    vector = x / 2;
    //Work width bootstrap 4
    $("#ship").addClass("position-absolute");
    $('#ship').css("left", "" + vector + "px");
}




function moveAndShot() {
    $("body").on("keydown", function (key) {
        const bind = key.which;
        const vValue = 5;
        if ((bind == 38)&&(start)) {
            //fire
            //before = clock.getTime();
            bullet();

        }
        switch (bind) {
            case 37:
                vector -= vValue;
                $("#ship").css("left", "" + vector + "px");
                break;

            case 39:
                vector += vValue;
                $("#ship").css("left", "" + vector + "px");
                break;

            default: console.log("Ready to fire!");

        }
        key.preventDefault();
    });
}

/* Just an older version
function shot(){
    $("body").keyup(function(attack){
        const fire = attack.which;

        if((fire==38)&&(end==1))
        {
              //fire
             bullet();
        }
    })
}
*/
function createEnemies() {

    for (i = 1; i < count; i++) {

        $("<div class='float-left some-space'><img id='enemy" + i + "' src='img/enemy" + lvl + ".png' alt='enemy' class='enemy' height='32px' width='74' /></div>").appendTo("#upper");
        
        console.log("Stworzono jednostkę: "+"#enemy"+i);
          
    }
    
    width = 74/2;
    
    height = 32/2;

    
}


function bullet() {
    //Fire!!!
    
   
        shotNr += 1;
        
        bottom = 1;
        

        $("<img id='shot" + shotNr + "' class='bullet' src='img/r_bullet.png' alt='bullet' height='72px' />").appendTo("#bullet-contain");
        $("#shot"+shotNr).addClass("position-absolute");
        $("#shot"+shotNr).css("left", "" + vector + "px");
        start = false;
        detectCollision();
        fire();
        

}

function fire() {

    // now = clock.getTime();
    // between = now - before;
    
    bottom = bottom + 4;
    
    $("#shot" + shotNr).css("bottom", "" + bottom + "px");

    if (bottom >= 600) {
        $("#shot" + shotNr).remove();
       
        start = true;

    }

   
        window.requestAnimationFrame(fire);
        
        }
 
function detectCollision() {
    for(var o=1; o<count; o++) {
        offsetBullet = $("#shot"+shotNr).offset();
        pos[o] = $("#enemy" + o).offset();
        if(offsetBullet.left>pos[o].left && offsetBullet.left<pos[o].left+width && offsetBullet.top>pos[o].top && offsetBullet.top<pos[o].top+height) {
            $("#shot" + shotNr).remove();
            console.log("Zestrzelono wrogi statek:"+"#enemy"+o);
            $("#enemy"+o).remove();
            start = true;
        
        }
}
window.requestAnimationFrame(detectCollision);
}



    /*
    Wykrywanie Kolizji/Colission detection:
-----------------------------------------------------
The x position of the ball is greater than the x position of the brick.
The x position of the ball is less than the x position of the brick plus its width.
The y position of the ball is greater than the y position of the brick.
The y position of the ball is less than the y position of the brick plus its height.
-----------------------------------------------------
piłka = obiekt
brick = element
Położenie poziome obiektu jest większe od pozycji poziomej elementu.
Jednak położenie poziome obiektu jest mniejsze od sumy pozycji poziomej elementu i jego szerokości.
Położenie pionowe obiektu jest większe od pozycji pionowej elementu.
Jakkolwiek położenie pionowe obiektu jest mniejsze od sumy pozycji pionowej elementu i jego wysokości. 
*/